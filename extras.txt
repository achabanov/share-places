OLD Podfile:


# Uncomment the next line to define a global platform for your project
# platform :ios, '9.0'

target 'shareplaces' do
  # Uncomment the next line if you're using Swift or would like to use dynamic frameworks
  # use_frameworks!

  # Pods for shareplaces

  target 'shareplaces-tvOSTests' do
    inherit! :search_paths
    # Pods for testing
  end

  target 'shareplacesTests' do
    inherit! :search_paths
    # Pods for testing
  end

end

target 'shareplaces-tvOS' do
  # Uncomment the next line if you're using Swift or would like to use dynamic frameworks
  # use_frameworks!

  # Pods for shareplaces-tvOS

  target 'shareplaces-tvOSTests' do
    inherit! :search_paths
    # Pods for testing
  end

end
